/*

Copyright 2020 The Vouch Proxy Authors.
Use of this source code is governed by The MIT License (MIT) that
can be found in the LICENSE file. Software distributed under The
MIT License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
OR CONDITIONS OF ANY KIND, either express or implied.

*/

package handlers

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"github.com/vouch/vouch-proxy/pkg/cfg"
	"github.com/vouch/vouch-proxy/pkg/cookie"
	"github.com/vouch/vouch-proxy/pkg/domains"
	"github.com/vouch/vouch-proxy/pkg/jwtmanager"
	"github.com/vouch/vouch-proxy/pkg/responses"
	"github.com/vouch/vouch-proxy/pkg/structs"
)

var (
	errSessionNotFound = errors.New("/auth could not retrieve session")
	errInvalidState    = errors.New("/auth the state nonce returned by the IdP does not match the value stored in the session")
	errURLNotFound     = errors.New("/auth could not retrieve URL from session")
)

var UserFQDNAcess map[string][]string = make(map[string][]string)

// CallbackHandler /auth
// - validate info from oauth provider (Google, GitHub, OIDC, etc)
// - issue jwt in the form of a cookie
func CallbackHandler(w http.ResponseWriter, r *http.Request) {
	log.Debug("/auth")
	// Handle the exchange code to initiate a transport.

	session, err := sessstore.Get(r, cfg.Cfg.Session.Name)
	if err != nil {
		responses.Error400(w, r, fmt.Errorf("/auth %w: could not find session store %s", err, cfg.Cfg.Session.Name))
		return
	}

	// is the nonce "state" valid?
	queryState := r.URL.Query().Get("state")
	if session.Values["state"] != queryState {
		responses.Error400(w, r, fmt.Errorf("/auth Invalid session state: stored %s, returned %s", session.Values["state"], queryState))
		return
	}

	// did the IdP return an error when they redirected back to /auth
	errorIDP := r.URL.Query().Get("error")
	if errorIDP != "" {
		errorDescription := r.URL.Query().Get("error_description")
		responses.Error401(w, r, fmt.Errorf("/auth Error from IdP: %s - %s", errorIDP, errorDescription))
		return
	}

	user := structs.User{}
	customClaims := structs.CustomClaims{}
	ptokens := structs.PTokens{}

	if err := getUserInfo(r, &user, &customClaims, &ptokens); err != nil {
		responses.Error400(w, r, fmt.Errorf("/auth Error while retreiving user info after successful login at the OAuth provider: %w", err))
		return
	}
	log.Debugf("/auth Claims from userinfo: %+v", customClaims)
	//getProviderJWT(r, &user)
	log.Debug("/auth CallbackHandler")
	log.Debugf("/auth %+v", user)

	// get the originally requested URL so we can send them on their way
	requestedURL := session.Values["requestedURL"].(string)
	log.Debugf("CallbackHandler() :: User= %s and App= %s", user.Username, requestedURL)

	// verify / authz the user
	if ok, err := verifyUser(user, requestedURL); !ok {
		responses.Error403(w, r, fmt.Errorf("/auth User is not authorized: %w . Please try again or seek support from your administrator", err))
		return
	}

	// SUCCESS!! they are authorized

	// issue the jwt
	u, err := url.Parse(requestedURL)
	if err != nil {
		responses.Error400(w, r, fmt.Errorf("/auth Error while parsing requested url: %w", err))
		return
	}

	userid := user.Email
	var fqdns []string
	if allowed, ok := UserFQDNAcess[userid]; ok {
		log.Infof("User %s is already seen for fqdns %s\n", userid, allowed)
		fqdns = append(allowed, u.Host)
		UserFQDNAcess[userid] = fqdns
	} else {
		log.Infof("User %s seen for first time\n", userid)
		UserFQDNAcess[userid] = []string{u.Host}
		fqdns = UserFQDNAcess[userid]
	}
	log.Infof("User accessing %s\n", u.Host)
	tokenstring := jwtmanager.CreateUserTokenString(user, customClaims, ptokens, fqdns)
	cookie.SetCookie(w, r, tokenstring)

	if requestedURL != "" {
		// clear out the session value
		session.Values["requestedURL"] = ""
		session.Values[requestedURL] = 0
		session.Options.MaxAge = -1
		if err = session.Save(r, w); err != nil {
			log.Error(err)
		}

		responses.Redirect302(w, r, requestedURL)
		return
	}
	// otherwise serve an error (why isn't there a )
	responses.RenderIndex(w, "/auth "+tokenstring)
}

// verifyUser validates that the domains match for the user
func verifyUser(u interface{}, requestedURL string) (bool, error) {

	user := u.(structs.User)
	var cfg_info map[string]interface{}

	data, err := ioutil.ReadFile("/etc/airgap/config.json")
	if err != nil {
		log.Debugf("Could not read config file %s ", err)
	}

	json.Unmarshal(data, &cfg_info)
	email := cfg_info["auth_email"].(string)
	password := cfg_info["auth_password"].(string)
	server_url := cfg_info["server_url"].(string)
	log.Debugf("verifyUser() : email : %s password : %s server_url :  %s ", email, password, server_url)

	log.Debugf("verifyUser() : Domains %d", len(cfg.Cfg.Domains))
	switch {

	// AllowAllUsers
	case cfg.Cfg.AllowAllUsers:
		log.Debugf("verifyUser: Success! skipping verification, cfg.Cfg.AllowAllUsers is %t", cfg.Cfg.AllowAllUsers)
		return true, nil

	// WhiteList
	case len(cfg.Cfg.WhiteList) != 0:
		for _, wl := range cfg.Cfg.WhiteList {
			if user.Username == wl {
				log.Debugf("verifyUser: Success! found user.Username in WhiteList: %s", user.Username)
				return true, nil
			}
		}
		return false, fmt.Errorf("verifyUser: user.Username not found in WhiteList: %s", user.Username)

	// TeamWhiteList
	case len(cfg.Cfg.TeamWhiteList) != 0:
		for _, team := range user.TeamMemberships {
			for _, wl := range cfg.Cfg.TeamWhiteList {
				if team == wl {
					log.Debugf("verifyUser: Success! found user.TeamWhiteList in TeamWhiteList: %s for user %s", wl, user.Username)
					return true, nil
				}
			}
		}
		return false, fmt.Errorf("verifyUser: user.TeamMemberships %s not found in TeamWhiteList: %s for user %s", user.TeamMemberships, cfg.Cfg.TeamWhiteList, user.Username)

	// Domains
	case len(cfg.Cfg.Domains) != 0:
		if domains.IsUnderManagement(user.Email) {
			request1Body, err := json.Marshal(map[string]string{
				"email":    email,
				"password": password,
			})

			timeout := time.Duration(10 * time.Second)
			client := http.Client{
				Timeout: timeout,
			}

			req1_url := server_url + "/api/v2/auth/login"
			log.Debugf("req1_url %s", req1_url)
			request1, err := http.NewRequest("POST", req1_url, bytes.NewBuffer(request1Body))
			request1.Header.Set("content-type", "application/json")
			mano_auth_resp, err := client.Do(request1)
			if err != nil {
				log.Debugf("verifyUser() : Get Authorization Token Failed for USer=%s; URL= %s, err=%s", user.Username, requestedURL, err)
			}
			defer mano_auth_resp.Body.Close()
			resp, err := ioutil.ReadAll(mano_auth_resp.Body)
			resp_map := make(map[string]map[string]string)
			json.Unmarshal(resp, &resp_map)
			token_string := resp_map["result"]["token"]
			bearer_token := "Bearer " + token_string
			log.Debugf("verifyUser() : Authorization Token Returned by Mano = %s ", bearer_token)

			req3_url := server_url + "/api/v3/access/group-policies/check-access"
			log.Debugf("req3_url %s", req3_url)
			log.Debugf("verifyUser() : Check Access for User=%s; URL= %s ", user.Email, requestedURL)
			request3, err := http.NewRequest("GET", req3_url, nil)
			request3.Header.Set("content-type", "application/json")
			request3.Header.Add("Authorization", bearer_token)
			q := request3.URL.Query()
			q.Add("user_email", user.Email)
			q.Add("application_url", requestedURL)
			request3.URL.RawQuery = q.Encode()
			get_access, err := client.Do(request3)
			if err != nil {
				log.Debugf("verifyUser() : Access  Failed for USer=%s; URL= %s, err=%s", user.Username, requestedURL, err)
			}
			defer get_access.Body.Close()
			access, err := ioutil.ReadAll(get_access.Body)
			access_str := string(access)
			log.Debugf("verifyUser() : Access Response from Controller : %s ", access_str)
			r := make(map[string]bool)
			json.Unmarshal(access, &r)
			tf := r["result"]
			log.Debugf("verifyUser() : Access Boolean : %v", tf)
			if !tf {
				return false, fmt.Errorf("verifyUser: Email %s Does not have Access to %s Application", user.Email, requestedURL)
			}

			log.Debugf("verifyUser() : Success! Email %s has access to %s Application", user.Email, requestedURL)
			return true, nil

		}

		return false, fmt.Errorf("verifyUser: Email %s is not within a "+cfg.Branding.FullName+" managed domain", user.Email)
	// nothing configured, allow everyone through
	default:
		log.Warn("verifyUser: no domains, whitelist, teamWhitelist or AllowAllUsers configured, any successful auth to the IdP authorizes access")
		return true, nil
	}
}

func getUserInfo(r *http.Request, user *structs.User, customClaims *structs.CustomClaims, ptokens *structs.PTokens) error {
	return provider.GetUserInfo(r, user, customClaims, ptokens)
}
